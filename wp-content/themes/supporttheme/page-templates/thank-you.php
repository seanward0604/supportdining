<?php
/*
Template Name: Thank You
*/
get_header(); ?>

<section class="home top">
	<div style="background: url('<?php the_field('hero'); ?>'); background-size:cover; height: 300px; background-position: top center; background-repeat: no-repeat;">
		
	</div>
	<div class='grid-x'>
			<div class='small-12 cell'>
				<div class="logo-container">
				<?php 
					$logo = get_field('logo');
					if( !empty( $logo ) ): ?>
						<img class="logo load-hidden reveal-up" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>" />
					<?php endif; ?>
				</div>
			</div>
		</div>

</section>


<section class="home intro">
	<div class='grid-container'>
		<div class='grid-x'>
			<div class='small-12 medium-10 medium-offset-1 cell'>
				<?php if ( get_field('intro') ) : ?>
					<div class="load-hidden reveal-up">
						<?php echo get_field('intro'); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class='small-12 medium-10 medium-offset-1 text-center cell'>
				<a href="<?php echo site_url(); ?>" class="button reveal-up load-hidden" style="margin: 25px auto;">Home</a>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class='grid-container'>
		<div class='grid-x'>

			<div class='small-12 text-center cell'>
			<?php 
					$logo = get_field('logo');
					if( !empty( $logo ) ): ?>
						<img class="logo load-hidden reveal-up" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>" />
					<?php endif; ?>
			</div>
			<div class="small-12 cell text-center reveal-up load-hidden">
				<p style="font-size: 12px;color:black;margin-top:50px; margin-bottom:10px;">Powered By</p>
				<a href="https://redirectdigital.com/" target="_blank"><img style="width: 100px; margin-top: 5px;" class="reveal-up load-hidden" src="https://supportsaltlakedining.com/wp-content/uploads/2020/03/templogo.png" alt="Redirect Digital"></a>
			</div>
		</div>
	</div>
</footer>


<?php get_footer() ?>