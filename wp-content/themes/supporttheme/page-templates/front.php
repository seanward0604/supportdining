<?php
/*
Template Name: Front
*/
get_header(); ?>

<section class="home top">
	<div style="background: url('<?php the_field('hero'); ?>'); background-size:cover; height: 300px; background-position: top center; background-repeat: no-repeat;">
		
	</div>
	<div class='grid-x'>
			<div class='small-12 cell'>
				<div class="logo-container">
				<?php 
					$logo = get_field('logo');
					if( !empty( $logo ) ): ?>
						<img class="logo load-hidden reveal-up" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>" />
					<?php endif; ?>
				</div>
			</div>
		</div>
</section>

<section class="home intro">
	<div class='grid-container'>
		<div class='grid-x'>
			<div class='small-12 medium-10 medium-offset-1 cell'>
				<?php if ( get_field('intro') ) : ?>
					<div class="load-hidden reveal-up">
						<?php echo get_field('intro'); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class='small-12 medium-10 medium-offset-1 text-center cell'>
				<a href="#sign-up" class="button reveal-up load-hidden" style="margin: 25px auto;">Add Your Restaurant</a>
			</div>
		</div>
	</div>
</section>

<section class="home filter">
    <div class="grix-container">
	<div class="finder-select" style="width: 100%;">
		<div class="grid-x grid-padding-x">
			<div class="small-12 large-2 cell"></div>
			<div class="small-12 large-4 reveal-up load-hidden cell">
					<?php if ( have_rows('location_search') ) : ?>
						<select class="select-city" name="type" id="type">
							<option value="restaurant-container">Select Location</option>
						<?php while( have_rows('location_search') ) : the_row(); ?>
							<option value="<?php the_sub_field('value'); ?>"><?php the_sub_field('title') ?></option>
						<?php endwhile; ?>
						</select>
					<?php endif; ?>
			</div>
			<div class="small-12 large-4 reveal-up load-hidden cell">
				<?php if ( have_rows('cuisine_search') ) : ?>
					<select class="select-cuisine" name="type" id="type">
						<option value="restaurant-container">Select Cuisine</option>
						<?php while( have_rows('cuisine_search') ) : the_row(); ?>
							<option value="<?php the_sub_field('value'); ?>"><?php the_sub_field('title') ?></option>
						<?php endwhile; ?>
						</select>
					<?php endif; ?>
			</div>
			<div class="small-12 large-2 cell"></div>
		</div>
	</div>
    </div>
</section>


<?php
	$args = array(
	'post_type' => 'restaurant',
	);
	$query = new WP_Query ($args);
?>
<section class="home restaurants">
	<div class='grid-container'>
		<div class='grid-x load-hidden reveal-up'>
			<?php if( $query->have_posts() ) : while($query->have_posts() ) : $query->the_post(); ?>
				<?php $location = get_field('location'); ?>
                <?php $cuisine = get_field('cuisine'); ?>
				<div class='small-6 medium-4 large-2 restaurant-container <?php echo $location ?> <?php echo $cuisine ?> cell'>
					<div class="restaurant">
						<?php $thumbnail = get_the_post_thumbnail_url(); ?>
						<?php if( !empty($thumbnail) ): ?>
							<div class="image-container"><img class="lazy" data-src="<?php echo $thumbnail; ?>" alt="<?php the_title(); ?>" /></div>
						<?php endif; ?>
						<div class="order">
							<?php $order = get_field('order'); ?>
							<?php if (  $order == "Online") : ?>
								<h6>Order Online</h6>
								<a class="button" target="_blank" href="<?php echo get_field('link'); ?>">Order Now</a>
							<?php endif; ?>
							<?php if (  $order == "Call") : ?>
								<h6>Call To Order</h6>
								<h5><?php echo get_field('phone'); ?></h5>
							<?php endif; ?>
							<?php 
								$website = get_field('website');
								if( $website ): 
									$website_url = $website['url'];
									$website_title = $website['title'];
									$website_target = $website['target'] ? $website['target'] : '_self';
									?>
									<a class=website href="<?php echo esc_url( $website_url ); ?>" target="<?php echo esc_attr( $website_target ); ?>"><?php echo esc_html( $website_title ); ?></a>
								<?php endif; ?>
						</div>
					</div>
				</div>
			<?php endwhile; endif; wp_reset_postdata(); ?>
				<div class="small-12 cell error-row text-center hide">
					<h3>Sorry, no restaurants match your search.</h3>
				</div>
        
			<div class='small-12 text-center cell' style="border-bottom: overflow: hidden;">
				<p>Menus and ordering information can be found at respective businesses websites.</p>
			</div>
		</div>
	</div>
</section>

<section class="home sign-up" id="sign-up">
	<div class='grid-container'>
		<div class='grid-x'>
			<div class='small-12 medium-8 large-6 medium-offset-2 large-offset-3 cell'>
				<?php if ( get_field('form') ) : ?>
					<?php echo get_field('form'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class='grid-container'>
		<div class='grid-x'>
			<div class='small-12 text-center cell'>
			<?php 
				$logo = get_field('logo');
				if( !empty( $logo ) ): ?>
					<img class="logo" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>" />
				<?php endif; ?>
			</div>
			<div class="small-12 cell text-center ">
				<p style="font-size: 12px;color:black;margin-top:50px; margin-bottom:10px;">Powered By</p>
				<a href="https://redirectdigital.com/" target="_blank"><img style="width: 100px; margin-top: 5px;" class="" src="<?php echo site_url(); ?>/wp-content/uploads/2020/03/RedirectLogo.png" alt="Redirect Digital"></a>
			</div>
		</div>
	</div>
</footer>

<?php get_footer();
