import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

$(document).foundation();

$(document).ready(function(){

    //////////////////////////////////////////////////////////////
    /////////////////      Global Inits  /////////////////////////
    //////////////////////////////////////////////////////////////

    window.sr = ScrollReveal({ reset: false });  

    if( $(window).width() <= 1023 ){
        $('.news-menu').removeClass('vertical');
    }


    //////////////////////////////////////////////////////////////
    /////////////////      Navigation  ///////////////////////////
    //////////////////////////////////////////////////////////////

    $('#nav-icon').click(function(){
        $(this).toggleClass('open');
        $('.nav-overlay').toggleClass('open');
        $('.site-header-main').toggleClass('nav-open');
        $('body').toggleClass('body-nav-open');
    });


    //////////////////////////////////////////////////////////////
    /////////////////      Animations   //////////////////////////
    //////////////////////////////////////////////////////////////


    // //Home Hero
    // sr.reveal('.home-hero  .headline h2', {  
    //     origin: 'right', 
    //     distance: '200px',
    //     duration: 800,
    //     delay: 300
    // });
    // sr.reveal('.home-hero  .headline p', {  
    //     origin: 'top', 
    //     distance: '50px',
    //     duration: 800,
    //     delay: 700
    // });
    // sr.reveal('.home-hero  .headline .button', {  
    //     origin: 'top', 
    //     distance: '50px',
    //     duration: 800,
    //     delay: 1200
    // });
    // sr.reveal('.home-hero .fa-arrow-down', {  
    //     origin: 'top', 
    //     distance: '25px',
    //     duration: 500,
    //     delay: 1700
    // });
    // sr.reveal('.clock', {  
    //     duration: 2500,
    //     delay: 1700
    // });


    // Hero
    sr.reveal('.hero  .headline', {  
        origin: 'top', 
        distance: '100px',
        duration: 800,
        delay: 100
    });
    sr.reveal('.hero .pattern', {  
        origin: 'bottom', 
        distance: '100px',
        duration: 1000,
        delay: 300
    });
    sr.reveal('.hero .foreground', {  
        origin: 'right', 
        distance: '100px',
        duration: 1000,
        delay: 800
    });
    sr.reveal('.hero .fa-arrow-down', {  
        origin: 'top', 
        distance: '25px',
        duration: 500,
        delay: 1200
    });
    // Reveal Classes
    sr.reveal('.reveal-right', { 
        origin: 'right', 
        distance: '75px',
        duration: 1000
    });
    sr.reveal('.reveal-up', {  
        origin: 'bottom', 
        distance: '75px',
        duration: 1000
    });
    sr.reveal('.reveal-left', { 
        origin: 'left', 
        distance: '75px',
        duration: 1000
    });
    sr.reveal('.reveal-down', {  
        origin: 'top', 
        distance: '75px',
        duration: 1000
    });
    sr.reveal('.reveal-fade', {  
        duration: 1000
    });


        // Select all links with hashes
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
          // On-page links
          if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
          ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
              // Only prevent default if animation is actually gonna happen
              event.preventDefault();
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 1000, function() {
                // Callback after animation
                // Must change focus!
                var $target = $(target);
                $target.focus();
                if ($target.is(":focus")) { // Checking if the target was focused
                  return false;
                } else {
                  $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                  $target.focus(); // Set focus again
                };
              });
            }
          }
        });





      $('.select-cuisine').on('change', function() {
          filterMouldings();
      });
      $('.select-city').on('change', function() {
          filterMouldings();
      });

  
      function filterMouldings(){
          var cuisine = $('.select-cuisine').val();
          var city = $('.select-city').val();
          var i = 0;
          $( '.restaurant-container' ).each(function() {
              if ( $(this).hasClass( cuisine ) && $(this).hasClass( city ) ){
                  $(this).removeClass('hide');
                  i++;
              }else{
                  $(this).addClass('hide');
              }
          });
          if(i == 0){
              $('.error-row').removeClass('hide');
          } else {
              $('.error-row').addClass('hide');
          }
      }


});